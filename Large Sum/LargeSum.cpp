//file		:	LargeSum.cpp
//author	:	steaKK

#include "LargeSum.h"

LargeSum::LargeSum() {

}

LargeSum::LargeSum(int _size) {
	ifstream file;
	file.open("file.in");
	string s = "";
	while(!file.eof()) {
		file >> s;
		data.push_back(s);
	}
	file.close();
}

LargeSum::LargeSum(const LargeSum& _LargeSum) {
	data = _LargeSum.data;
}

LargeSum& LargeSum::operator=(const LargeSum& _LargeSum) {
	data = _LargeSum.data;
	return *this;
}

LargeSum::~LargeSum() {

}

string LargeSum::solve() {
	BigInt::Rossi result("0", BigInt::DEC_DIGIT);
	for(int i=0;i<data.size();i++) {
		cout << result.toStrDec() << endl;
		BigInt::Rossi temp(data[i], BigInt::DEC_DIGIT);
		result = result + temp;
	}
	return result.toStrDec();
}
