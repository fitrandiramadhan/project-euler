//file		:	LargeSum.h
//author	:	steaKK

#ifndef LargeSum_H
#define LargeSum_H

#include <iostream>
#include <fstream>
#include <vector>

#include "lib/bigInt.h"

using namespace std;

class LargeSum {
public:
	LargeSum();
	LargeSum(int);
	LargeSum(const LargeSum&);
	LargeSum& operator=(const LargeSum&);
	~LargeSum();

	int get_size();
	void set_size(int);
	string get_data(int);
	void set_data(int,string);

	string solve();

private:
	static const int DEFAULT_DATA_SIZE = 10;

	vector<string> data;
};

#endif
