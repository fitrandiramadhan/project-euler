//file		:	LatticePath.h
//author	:	steaKK

#ifndef LatticePath_H
#define LatticePath_H

#include <iostream>
#include <vector>

using namespace std;

class LatticePath {
public:
	LatticePath();
	LatticePath(int);
	LatticePath(const LatticePath&);
	LatticePath& operator=(const LatticePath&);
	~LatticePath();


private:
	vector<vector<int> > data;
};

#endif
